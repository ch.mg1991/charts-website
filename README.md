## VISUALIZAÇÃO

Para vizualizar o projeto basta clicar [aqui](https://chartsteste.000webhostapp.com/)

## Name
CHARTS

## Description
Modelo de website desenvolvido em PHP, JQUERY, HTML5, CSS3. Um site proposto a vender NFTs. Totalmente resposivo.
(Foi utilizado o plugin "jquery.particleground.js" para o background)

## Installation
Para rodar o website localmente é necessário um servidor Apache, recomendo utizar o XAMPP. Após a instalação, criar uma pasta dentro de "htdocs" (onde foi instalado o XAMPP) colocar o arquivos dentro desta pasta. Logo após, abrir o navegador e acessar (localhost/nomedapastacriada/) (Lembre-se de inicializar o servidor apache no painel de controle do XAMPP)

## Authors
Carlos Henrique Venâncio Filho

## License
Livre
